struct node{
  int value;
  struct node *next;
};

void push(int);
void pop(void);
int top(void);
int isEmpty(void);
int stack_size(void);
