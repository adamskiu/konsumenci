OUTPROG = konsumenci
CC = gcc
CFLAGS = -g -Wall -Werror -Iinclude
LDFLAGS = -pthread
SOURCES = src/main.c src/my_stack.c

all:	
	$(CC) $(CFLAGS) $(LDFLAGS) $(SOURCES) -o $(OUTPROG)

clean:
	rm -f $(OUTPROG)



