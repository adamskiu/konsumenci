#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "my_stack.h"

pthread_mutex_t MTX = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t COND = PTHREAD_COND_INITIALIZER;
int RUN = 1;

void *thread_func(void *thread_id);
int main(int argc, char **argv)
{
  if (argc != 3){
    printf("Usage: ./konsumenci thread_count time.\n");
    return 1;
  }
  int result;
  int thread_count = atoi(argv[1]);
  pthread_t *tids = (pthread_t*) malloc(thread_count * sizeof(pthread_t));

  int time_to_work = atoi(argv[2]);
  time_t start, stop;
  start = time(0);
  do{ // wrzucaj losowe liczby na stos
    push(rand() % 1001);
    stop = time(0);
  } while (stop - start < time_to_work);

  printf("Dodałem do stosu %d elementów, tworzę wątki.\n", stack_size());

  int i;
  for (i = 0; i < thread_count; i++){
    result = pthread_create(&tids[i], 0, thread_func, &i);
    if (result){
      perror("pthread_create");
      return 2;
    }
  }

  start = time(0);
  do{
    stop = time(0);
  } while (stop - start < time_to_work);

  // Tutaj należy skończyć działalność konsumentów i joinować
  RUN = 0;
  for (i = 0; i < thread_count; ++i){
    result = pthread_join(tids[i],0 );
    if (result){
      perror("pthread_join");
      exit(3);
    }
  }
  free(tids);
  return 0;
}

void *thread_func(void *thread_id)
{
  int value, id = *(int *)thread_id;
  while( !isEmpty() && RUN){
    value = top();
    pop();
    printf("thread %d: %d\n",id, value);
  }

  return 0;
}
