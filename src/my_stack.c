#include "my_stack.h"
#include <stdlib.h>

typedef struct node node;

struct node *head = NULL;
struct node *stackTop = NULL;
static int size;

void push(int value)
{
  if (!head){
    head = (node*) malloc(sizeof(node));
    head->next = NULL;
    stackTop = head;
  }

  // Stwórz nowy węzeł, niech wskazuje na szczyt stosu
  // i jednocześnie niech ten węzeł stanie się teraz
  // szczytem stosu
  node *temp = (node*) malloc(sizeof(node));
  temp->value = value;
  temp->next = stackTop;
  stackTop = temp;
  ++size;
}

void pop(void)
{
  /* jeśli jesteśmy na dnie stosu */
  if (stackTop == head)
    return;
  /* W innym wypadku usuńmy element ze szczytu i zejdźmy jeden
     element w dół */
  node *temp = stackTop;
  stackTop = stackTop->next;
  free(temp);
  --size;
}

int top(void)
{
  return stackTop->value;
}

int isEmpty(void)
{
  return stackTop == head;
}

int stack_size(void)
{
  return size;
}
